package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        final EditText edKodeCari = (EditText)findViewById(R.id.txtEditKodeCariMkl);
        final EditText edUpdtNama = (EditText)findViewById(R.id.txtEditNamaMkl);
        final EditText edUpdtKode = (EditText)findViewById(R.id.txtEditKodeBaruMkl);
        final EditText edUpdtHari = (EditText)findViewById(R.id.txtEditHariMkl);
        final EditText edUpdtSesi = (EditText)findViewById(R.id.txtEditSesiMkl);
        final EditText edUpdtSks = (EditText)findViewById(R.id.txtEditSksMkl);
        Button btnUpdateMkl = (Button) findViewById(R.id.btnUpdateMkl);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        if (data!=null){
            edKodeCari.setText(data.getStringExtra("kode"));
            edUpdtNama.setText(data.getStringExtra("nama"));
            edUpdtKode.setText(data.getStringExtra("kode"));
            edUpdtHari.setText(data.getStringExtra("hari"));
            edUpdtSesi.setText(data.getStringExtra("sesi"));
            edUpdtSks.setText(data.getStringExtra("sks"));

            btnUpdateMkl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Please Wait");
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> update = service.delete_matkul(
                            edKodeCari.getText().toString(),
                            "72180184"
                    );
                    update.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            Toast.makeText(MatkulUpdateActivity.this, "Berhasil disimpan",Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MatkulUpdateActivity.this,"Eror", Toast.LENGTH_LONG);
                        }
                    });
                    Call<DefaultResult>addMatkul = service.add_matkul(
                            edUpdtNama.getText().toString(),
                            edUpdtKode.getText().toString(),
                            edUpdtHari.getText().toString(),
                            edUpdtSesi.getText().toString(),
                            edUpdtSks.getText().toString(),
                            "72180184"
                    );
                    addMatkul.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(MatkulUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MatkulUpdateActivity.this,"Error",Toast.LENGTH_LONG);
                        }
                    });
                }
            });
        }



    }
}