package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.UtsLogin;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class LoginMainActivity extends AppCompatActivity {
    ProgressDialog pd;
    String isLogin="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        EditText loginNim =(EditText)findViewById(R.id.loginNim);
        EditText loginPass =(EditText)findViewById(R.id.loginPassword);
        Button btnLogin =(Button)findViewById(R.id.btnLogin);
        pd=new ProgressDialog(LoginMainActivity.this);

        SharedPreferences pref = LoginMainActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        isLogin =pref.getString("isLogin","0");
        if(isLogin.equals("1")) {
            Intent intent = new Intent(LoginMainActivity.this, MainMenuActivity.class);
            startActivity(intent);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Logging In");
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<UtsLogin>> login= service.Login(
                        loginNim.getText().toString(),
                        loginPass.getText().toString()
                );
                login.enqueue(new Callback<List<UtsLogin>>() {
                    @Override
                    public void onResponse(Call<List<UtsLogin>> call, Response<List<UtsLogin>> response) {
                        if (response.body().size() != 0) {
                            editor.putString("isLogin", "1");
                            editor.commit();
                            pd.dismiss();
                            Intent intent = new Intent(LoginMainActivity.this, MainMenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Toast.makeText(LoginMainActivity.this, "success", Toast.LENGTH_LONG).show();
                        } else {
                            pd.dismiss();
                            Toast.makeText(LoginMainActivity.this, "failed", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<UtsLogin>> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(LoginMainActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}