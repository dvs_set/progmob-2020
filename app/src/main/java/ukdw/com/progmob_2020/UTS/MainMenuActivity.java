package ukdw.com.progmob_2020.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import ukdw.com.progmob_2020.R;

public class MainMenuActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        ImageButton btnMahasiswa = (ImageButton)findViewById(R.id.btnMahasiswa);
        ImageButton btnDosen = (ImageButton)findViewById(R.id.btnDosen);
        ImageButton btnMatkul = (ImageButton)findViewById(R.id.btnMatkul);
        androidx.appcompat.widget.Toolbar tbMainUts = (androidx.appcompat.widget.Toolbar)findViewById(R.id.toolbar2);
        setSupportActionBar(tbMainUts);

        btnMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this,MahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this,DosenGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this,MatkulActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        SharedPreferences pref = MainMenuActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        switch (item.getItemId()){
            case R.id.tbLogout:
                isLogin =pref.getString("isLogin","0");
                editor.putString("isLogin", "0");
                editor.commit();
                Intent intent = new Intent(MainMenuActivity.this, LoginMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}