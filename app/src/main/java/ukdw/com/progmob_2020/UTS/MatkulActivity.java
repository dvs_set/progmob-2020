package ukdw.com.progmob_2020.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.DosenAdapterUts;
import ukdw.com.progmob_2020.Adapter.MahasiswaAdapterUts;
import ukdw.com.progmob_2020.Adapter.MatkulAdapterUts;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulAdapterUts matkulAdapterUts;
    ProgressDialog pd;
    List<Matkul> matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul);
        rvMatkul = (RecyclerView)findViewById(R.id.rvActivityMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon bersabar");
        pd.show();
        androidx.appcompat.widget.Toolbar tbMatkulUts = (androidx.appcompat.widget.Toolbar)findViewById(R.id.tbMatkulUts);
        setSupportActionBar(tbMatkulUts);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class); //mengambil data
        Call<List<Matkul>> call = service.getMatkul("72180184");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapterUts = new MatkulAdapterUts(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulAdapterUts);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                Toast.makeText(MatkulActivity.this,"Error",Toast.LENGTH_LONG);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tbAdd:
                Intent intent = new Intent(MatkulActivity.this, MatkulAddActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}