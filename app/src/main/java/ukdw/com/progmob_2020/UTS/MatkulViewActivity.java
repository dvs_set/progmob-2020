package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import ukdw.com.progmob_2020.R;

public class MatkulViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_view);
        TextView tvKode = (TextView) findViewById(R.id.txtVwKodeMklUts);
        TextView tvNama = (TextView) findViewById(R.id.txtVwNamaMklUts);
        TextView tvHari = (TextView) findViewById(R.id.txtVwHariMklUts);
        TextView tvSesi = (TextView) findViewById(R.id.txtVwSesiMklUts);
        TextView tvSks = (TextView) findViewById(R.id.txtVwSksMklUts);

        Intent data = getIntent();
        if (data!=null){
            tvKode.setText(data.getStringExtra("kode"));
            tvNama.setText(data.getStringExtra("nama"));
            tvHari.setText(data.getStringExtra("hari"));
            tvSesi.setText(data.getStringExtra("sesi"));
            tvSks.setText(data.getStringExtra("sks"));
        }
    }
}