package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);

        final EditText addNamaMkl = (EditText)findViewById(R.id.txtAddNamaMkl);
        final EditText addKodeMkl = (EditText)findViewById(R.id.txtAddKodeMkl);
        final EditText addHariMkl = (EditText)findViewById(R.id.txtAddHariMkl);
        final EditText addSesiMkl = (EditText)findViewById(R.id.txtAddSesiMkl);
        final EditText addSksMkl = (EditText)findViewById(R.id.txtAddSksMkl);
        Button btnSimpanMkl = (Button)findViewById(R.id.btnSimpanMkl);
        pd = new ProgressDialog(MatkulAddActivity.this);

        btnSimpanMkl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               pd.setTitle("Mohon Menunggu");
               pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        addNamaMkl.getText().toString(),
                        addKodeMkl.getText().toString(),
                        addHariMkl.getText().toString(),
                        addSesiMkl.getText().toString(),
                        addSksMkl.getText().toString(),
                        "72180184"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "Data Berhasil Disimpan", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}