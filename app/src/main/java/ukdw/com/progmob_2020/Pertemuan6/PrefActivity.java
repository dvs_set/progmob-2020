package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.UTS.MainMenuActivity;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        final EditText loginNim = (EditText) findViewById(R.id.loginNimPref);
        final EditText loginPassword = (EditText) findViewById(R.id.loginPasswordPref);
        Button btnPref = (Button)findViewById(R.id.btnLoginPref);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        //membaca pref islogin apakah true atau false
        isLogin = pref.getString("isLogin","0");
        if(isLogin.equals("1")){
            Intent intent = new Intent(PrefActivity.this, MainMenuActivity.class);
            startActivity(intent);
        }

        //pengisian preference
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin","0");
                if(isLogin == "0"){
                    editor.putString("isLogin","1");
                    Intent intent = new Intent(PrefActivity.this, MainMenuActivity.class);
                    startActivity(intent);
                }
                editor.commit();
            }
        });

       /* @Override
        public boolean onCreateOptionsMenu(Menu menu){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu, menu);
            return super.onCreateOptionsMenu(menu);
        }
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            switch ()
            return super.onOptionsItemSelected(item);
        }*/

    }
}