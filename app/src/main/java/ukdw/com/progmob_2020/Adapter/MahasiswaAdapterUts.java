package ukdw.com.progmob_2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.UTS.UtsMhsUpdateActivity;
import ukdw.com.progmob_2020.UTS.UtsMhsViewActivity;

public class MahasiswaAdapterUts extends RecyclerView.Adapter<MahasiswaAdapterUts.ViewHolder>{
    private Context context;
    private List<Mahasiswa> mahasiswaList;
    ProgressDialog pd;

    public MahasiswaAdapterUts(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaAdapterUts(List<Mahasiswa> mahasiswaList)
    {
        this.mahasiswaList = mahasiswaList;
    }


    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_mahasiswa,parent,false);
        return new ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaAdapterUts.ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);
        holder.tvNamaMhs.setText(m.getNama());
        holder.tvEmailMhs.setText(m.getEmail());
        holder.mhs = m;
        pd = new ProgressDialog(holder.context1);
        holder.btnMhsUpDelViwUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Mahasiswa m = mahasiswaList.get(position);
                PopupMenu popupMenu = new
                        PopupMenu(holder.context1,holder.btnMhsUpDelViwUts);
                popupMenu.inflate(R.menu.menu_edit);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (menuItem.getItemId()){
                            case R.id.tbDelete:
                                pd.setTitle("Deleted on Process");
                                pd.show();
                                Call<DefaultResult> del=service.delete_mhs(m.getNim().toString(),"72180184");
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1,"Berhasil dihapus",Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1,"Gagal dihapus",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.tbView:
                                Intent mhsVw = new Intent(holder.context1, UtsMhsViewActivity.class);
                                mhsVw.putExtra("nim",m.getNim());
                                mhsVw.putExtra("nama",m.getNama());
                                mhsVw.putExtra("alamat",m.getAlamat());
                                mhsVw.putExtra("email",m.getEmail());
                                holder.context1.startActivity(mhsVw);
                                break;
                            case R.id.tbUpdate:
                                Intent mhsUpdt = new Intent(holder.context1, UtsMhsUpdateActivity.class);
                                mhsUpdt.putExtra("nim",m.getNim_cari());
                                mhsUpdt.putExtra("nim",m.getNim());
                                mhsUpdt.putExtra("nama", m.getNama());
                                mhsUpdt.putExtra("alamat", m.getAlamat());
                                mhsUpdt.putExtra("email", m.getEmail());
                                holder.context1.startActivity(mhsUpdt);
                        }
                        return false;
                    }
                });
            }
        });
    }
    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaMhs, tvEmailMhs, tvNoTelp;
        private ImageView btnMhsUpDelViwUts;
        Context context1;
        Mahasiswa mhs;

        public ViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            tvNamaMhs = itemView.findViewById(R.id.txtNama);
            tvEmailMhs = itemView.findViewById(R.id.txtEmail);
            btnMhsUpDelViwUts = itemView.findViewById(R.id.btnMhsUpDelViwUts);
            context1 = context;
        }
    }
}
