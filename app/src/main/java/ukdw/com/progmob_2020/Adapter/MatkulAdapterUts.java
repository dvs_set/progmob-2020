package ukdw.com.progmob_2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.UTS.DosenUpdateActivity;
import ukdw.com.progmob_2020.UTS.DosenViewActivity;
import ukdw.com.progmob_2020.UTS.MatkulUpdateActivity;
import ukdw.com.progmob_2020.UTS.MatkulViewActivity;

public class MatkulAdapterUts extends RecyclerView.Adapter<MatkulAdapterUts.ViewHolder>{
    private Context context;
    private List<Matkul> matkulList;
    ProgressDialog pd;

    public MatkulAdapterUts(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulAdapterUts(List<Matkul> matkulList)
    {
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_matkul,parent,false);
        return new ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MatkulAdapterUts.ViewHolder holder, int position) {
        Matkul mkl = matkulList.get(position);
        holder.tvNamaMatkul.setText(mkl.getNama());
        holder.tvKodeMatkul.setText(mkl.getKode());
        holder.mkl = mkl;
        pd = new ProgressDialog(holder.context3);
        holder.btnMtklVwDelUpUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Matkul mkl = matkulList.get(position);
                PopupMenu popupMenu = new
                        PopupMenu(holder.context3,holder.btnMtklVwDelUpUts);
                popupMenu.inflate(R.menu.menu_edit);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (menuItem.getItemId()){
                            case R.id.tbDelete:
                                pd.setTitle("Deleted on Process");
                                pd.show();
                                Call<DefaultResult> del=service.delete_matkul(mkl.getKode().toString(),"72180184");
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context3,"Berhasil dihapus",Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context3,"Gagal dihapus",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.tbView:
                                Intent mklVw = new Intent(holder.context3, MatkulViewActivity.class);
                                mklVw.putExtra("kode",mkl.getKode());
                                mklVw.putExtra("nama",mkl.getNama());
                                mklVw.putExtra("hari",mkl.getHari());
                                mklVw.putExtra("sesi",mkl.getSesi());
                                mklVw.putExtra("sks",mkl.getSks());
                                holder.context3.startActivity(mklVw);
                                break;
                            case R.id.tbUpdate:
                                Intent mklUpd = new Intent(holder.context3, MatkulUpdateActivity.class);
                                mklUpd.putExtra("kode_cari",mkl.getKode());
                                mklUpd.putExtra("nama",mkl.getNama());
                                mklUpd.putExtra("nidn", mkl.getKode());
                                mklUpd.putExtra("hari", mkl.getHari());
                                mklUpd.putExtra("sesi", mkl.getSesi());
                                mklUpd.putExtra("sks", mkl.getSks());
                                holder.context3.startActivity(mklUpd);
                        }
                        return false;
                    }
                });
            }
        });
    }
    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaMatkul, tvKodeMatkul;
        private ImageView btnMtklVwDelUpUts;
        Context context3;
        Matkul mkl;
        public ViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            tvNamaMatkul = itemView.findViewById(R.id.txtNamaMatkulUts);
            tvKodeMatkul = itemView.findViewById(R.id.txtKodeMatkulUts);
            btnMtklVwDelUpUts = itemView.findViewById(R.id.btnMtklVwDelUpUts);
            context3 = context;
        }
    }
}
